import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card } from 'antd';
import { CommonTable } from 'knk-react';
// 国际化
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import { ConfigProvider } from 'antd';

import Mock from '../../../mock'
Mock.bootstrap();

const genderName = {
    0: '男',
    1: '女'
};

class CommonTableDemo extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        let option = [
            {
                title: '用户ID',
                key: 'userId',
                dataIndex: 'userId'
            }, {
                title: '用户名称',
                key: 'userName',
                dataIndex: 'userName'
            }, {
                title: '性别',
                key: 'gender',
                dataIndex: 'gender',
                render: (text) => {
                    return genderName[text];
                }
            }, {
                title: '电子邮箱',
                key: 'email',
                dataIndex: 'email'
            }, {
                title: '出生日期',
                key: 'birthDate',
                dataIndex: 'birthDate'
            }
        ];

        return (
            <ConfigProvider locale={zh_CN}>
                <Card className="m-b" title={'用户列表'} >
                    <CommonTable columns={option} apiName='user-list' apiData={{}} isShowPagination={true}/>
                </Card>
            </ConfigProvider>
        );
    }
}

CommonTableDemo.propTypes = {
    form: PropTypes.object,
    onSubmit: PropTypes.func,
    defaultValue: PropTypes.object
};

export default CommonTableDemo;