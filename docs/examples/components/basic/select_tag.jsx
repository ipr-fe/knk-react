import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'antd';
import { SelectTag } from 'knk-react';

const FormItem = Form.Item;

const tags = [
    {
        text: '红色',
        value: '1'
    }, {
        text: '橙色',
        value: '2'
    }, {
        text: '黄色',
        value: '3'
    }
];
class SelectTagDemo extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {      
        const { getFieldDecorator } = this.props.form;
          
        return (
            <Form>
                <Row>
                    <Col span={8}>
                        <FormItem
                            label="Tags"
                        >
                            {getFieldDecorator('keyWord', {
                                initialValue: tags
                            })(
                                <SelectTag />
                            )}
                        </FormItem>
                    </Col>
                </Row>
            </Form>
        );
    }
}

SelectTagDemo.propTypes = {
    form: PropTypes.object.isRequired,
    onSubmit: PropTypes.func,
    defaultValue: PropTypes.object
};

export default Form.create()(SelectTagDemo);