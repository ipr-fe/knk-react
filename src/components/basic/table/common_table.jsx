/**
 * 通用表格
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Table, message, Spin } from 'antd';

import { fetch } from '@/common/api';
import { getPageData, isObjEmpty, filterEmptyData } from '@/common/tool';

class CommonTable extends Component {
    constructor(props) {
        super(props);
        this.handleGetData = this.handleGetData.bind(this);
        this.handleChangePage = this.handleChangePage.bind(this);
        this.state = {
            tableDate: [],
            loading: false
        };
        // 是否已加载
        this.open = true;
    }
    componentDidMount() {
        this.handleGetData({
            page: 1,
            pageSize: 10
        });
    }
    componentWillUnmount() {
        this.open = false;
    }
    handleChangePage(page, filters, sorter) {
        const { sortType } = this.props;
        let orders = {};
        if (!isObjEmpty(sorter)) {
            sortType.map((item) => {
                if (item.text == sorter.columnKey) {
                    orders = {
                        sortType: item.value,
                        isDesc: sorter.order === 'descend' ? 1 : 0
                    };
                }
            });
        }
        this.handleGetData(Object.assign({
            page: page.current,
            pageSize: page.pageSize
        }, orders));
    }
    render() {
        let { tableDate, loading } = this.state;
        const { tableTitle, columns, isShowPagination, rightLink, rightLinkTitle, pageSizeOptions, rowKeyFun, scroll } = this.props;
        let pageData;
     
        if(isShowPagination && pageSizeOptions) {
            pageData = Object.assign({}, getPageData(tableDate), {pageSizeOptions: pageSizeOptions});
        }else pageData = Object.assign({}, getPageData(tableDate));
       
        return (
            <Spin spinning={loading}>
                {tableTitle ? 
                    <h3>
                        {tableTitle}
                        {rightLink ? <a className="right-link pull-right" href={rightLink} target="_blank" rel="noopener noreferrer">{rightLinkTitle}</a> : null}
                    </h3> 
                    : null}
                <Table
                    columns={columns}
                    bordered={true}
                    pagination={isShowPagination ? pageData : false}
                    dataSource={tableDate.content}
                    onChange={this.handleChangePage}
                    rowKey={rowKeyFun}
                    scroll={scroll}
                >
                </Table>
            </Spin>
        );
    }
    handleGetData(data) {
        const {apiName, apiData, apiMethod } = this.props;
        this.setState({
            loading: true
        });
        let sendData = filterEmptyData(Object.assign({}, apiData, data));
        fetch(apiName, sendData, apiMethod)
            .then((data) => {
                // 判断如果卸载,则不再设置数据
                if(!this.open) return;
                if(!data.data || !data.data.content) data.data.content = [];
                data.data.content.map((item, inx) => {
                    item.key = inx;
                });
                this.setState({
                    loading: false,
                    tableDate: data.data
                });
            })
            .catch((err) => {
                if(!this.open) return;
                this.setState({
                    loading: false
                });
                message.error('获取数据失败:' + err.message);
            });
    }
}
CommonTable.propTypes = {
    rightLink: PropTypes.string, // table 右侧链接
    rightLinkTitle: PropTypes.string, // table 右侧链接文案
    tableTitle: PropTypes.string, // table名字
    columns: PropTypes.array.isRequired, //表格的配置项
    isShowBordered: PropTypes.bool, // table是否带有边框,
    isShowPagination: PropTypes.bool, // table是否展示分页
    apiName: PropTypes.string.isRequired, // API请求名,由于直接诶读取data.data的数组数据,所以该API不能使用分页
    apiData: PropTypes.object, // api请求附加的数据
    apiMethod: PropTypes.string, // api请求method,
    sortType: PropTypes.array, // table排序类型 [{ text: 'sortName', value: 'sortValue'}],
    pageSizeOptions: PropTypes.array,
    rowKeyFun: PropTypes.func,
    scroll: PropTypes.object,
};
export default CommonTable;