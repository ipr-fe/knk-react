import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { chanceTypes, users } from './data/db';

export default {
    /**
     * mock bootstrap
     */
    bootstrap() {
        let mock = new MockAdapter(axios);

        // mock success request
        mock.onGet('/success').reply(200, {
            msg: 'success'
        });

        // mock error request
        mock.onGet('/error').reply(500, {
            msg: 'failure'
        });

        //获取机会类型列表
        mock.onGet('/chance-type').reply(config => {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve([200, {
                        data: chanceTypes
                    }]);
                }, 1000);
            });
        });

        //获取用户列表
        mock.onGet('/user-list').reply(config => {
            const userName = config.params && config.params.userName || '';
            const page = config.params && config.params.page || 1;
            const pageSize = config.params && config.params.pageSize || 10;
            let userList = users.filter(user => {
                if (userName && user.userName.indexOf(userName) == -1) return false;
                return true;
            });
            let total = userList.length;
            userList = userList.filter((u, index) => index < pageSize * page && index >= pageSize * (page - 1));
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve([200, {
                        data: { content: userList, page: page, total: total, pageSize: pageSize}
                    }]);
                }, 1000);
            });
        });
    }
};