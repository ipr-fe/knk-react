import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'antd';
import { AjaxSelect } from 'knk-react';
import Mock from '../../../mock'
Mock.bootstrap();
const FormItem = Form.Item;

class AjaxSelectDemo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {}
        };
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {      
        const { getFieldDecorator } = this.props.form;
          
        return (
            <Form>
                <Row>
                    <Col span={8}>
                        <FormItem
                            label="机会类型"
                        >
                            {getFieldDecorator('chanceId', {
                                initialValue: ''
                            })(
                                <AjaxSelect 
                                    apiName='chance-type'
                                    apiData={{}}
                                    listName="机会类型"
                                    listValueKey="chanceId"
                                    listNameKey="chanceName" 
                                />
                            )}
                        </FormItem>
                    </Col>
                </Row>
            </Form>
        );
    }
}

AjaxSelectDemo.propTypes = {
    form: PropTypes.object.isRequired,
    onSubmit: PropTypes.func,
    defaultValue: PropTypes.object
};

export default Form.create()(AjaxSelectDemo);