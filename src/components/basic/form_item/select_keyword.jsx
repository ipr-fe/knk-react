/**
 * 关键字搜索
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Select, Input, Row, Col } from 'antd';
const Option = Select.Option;

const style = {
    width: '97%'
};

class SelectKeyWord extends Component {
    constructor(props) {
        super(props);
        this.handleChangeKeyWord = this.handleChangeKeyWord.bind(this);
        this.handleInput = this.handleInput.bind(this);
    }
    render() {
        let { keyWord, value, isDefaultOption, defaultval, defaultStr } = this.props;
        return (
            <Row>
                <Col span={11}>
                    <Select allowClear placeholder="请选择" style={style} value={value && value[0] ? value[0] : []} onChange={this.handleChangeKeyWord}>
                        {
                            isDefaultOption ? 
                                null :
                                <Option value= { defaultval || '' }>{ defaultStr || '请选择'}</Option>
                        }
                        
                        {keyWord.map((item) => {
                            return <Option key={item.value}  value={item.value}>{item.text}</Option>;
                        })}
                    </Select>
                </Col>
                <Col span={13}>
                    <Input style={style} value={value && value[1] ? value[1] : ''} onChange={this.handleInput} placeholder="请输入关键词"/>
                </Col>
            </Row>
        );
    }
    handleChangeKeyWord(data) {
        const {  onChange, value } = this.props;
        onChange([data, value && value[1] ? value[1] : '']);
    }
    handleInput(e) {
        const { value, onChange } = this.props;
        onChange([value[0], e.target.value]);
    }
}

SelectKeyWord.propTypes = {
    keyWord: PropTypes.array.isRequired,
    onChange: PropTypes.func,
    value: PropTypes.array,
    isDefaultOption: PropTypes.bool,
    defaultStr: PropTypes.string,
    defaultval: PropTypes.string,
};

export default SelectKeyWord;