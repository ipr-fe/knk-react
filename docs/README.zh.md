---
title: knk-react
hero:
  title: knk-react
  desc: 🏆 让中后台开发更简单
  actions:
    - text: Get Started →
      link: /guide
features:
  - icon: https://gw.alipayobjects.com/os/q/cms/images/k9ziitmp/13668549-b393-42a2-97c3-a6365ba87ac2_w96_h96.png
    title: 简单易用
    desc: 在 `antd` 基础上封装<br/> 即拿即用
  - icon: https://gw.alipayobjects.com/os/q/cms/images/k9ziik0f/487a2685-8f68-4c34-824f-e34c171d0dfd_w96_h96.png
    title: Ant Design
    link: https://ant.design/index-cn
    desc: 与 `antd` 写法一致<br/> 当前对应版本 `3.1.1`
  - icon: https://gw.alipayobjects.com/zos/antfincdn/CPoxyg4J2d/geography.png
    title: 国际化
    desc: 提供完备的国际化，与 Ant Design 体系打通
footer: ISC Licensed | Copyright © 2021-present<br />Powered by [ipr-fe](https://git.zhubajie.la/ipr-fe)
---

## 🍭 简介

以原生 `antd` 为基础，通过自定义 `css` 或 引入其他插件，实现中后台常用组件。

## 📐 组件看板

- 基础组件
  - [SelectKeyWord 选择关键字](https://ipr-fe.gitee.io/knk-react/components/basic/select_keyword)
  - [SelectTag 选择标签](https://ipr-fe.gitee.io/knk-react/components/basic/select_tag)
  - [AjaxSelect 异步选择](https://ipr-fe.gitee.io/knk-react/components/basic/ajax_select)
  - [CommonTable 通用表格](https://ipr-fe.gitee.io/knk-react/components/basic/common_table)

## 🖥 浏览器兼容性

- 现代浏览器和 Internet Explorer 11 (with [polyfills](https://stackoverflow.com/questions/57020976/polyfills-in-2019-for-ie11))
- [Electron](https://www.electronjs.org/)

| ![](https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png)<br>IE / Edge | ![](https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png)<br>Firefox | ![](https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png)<br>Chrome | ![](https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png)<br>Safari | ![](https://raw.githubusercontent.com/alrra/browser-logos/master/src/electron/electron_48x48.png)<br>Electron |
| ------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------- |
| IE11, Edge                                                                                             | last 2 versions                                                                                            | last 2 versions                                                                                         | last 2 versions                                                                                         | last 2 versions                                                                                               |