import { Component } from 'react';
import { tool } from 'knk-react';

class FilterEmptyDataDemo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                userId: 1,
                userName: 'artd',
                gender: '',
                address: null
            }
        };
    }

    render() {    
        const { data } = this.state;
        const filterData = tool.filterEmptyData(data);            
        return (
            <div>
                { JSON.stringify(filterData) }
            </div>
        );
    }
}

export default FilterEmptyDataDemo;