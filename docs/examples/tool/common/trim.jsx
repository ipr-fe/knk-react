import { Component } from 'react';
import { tool } from 'knk-react';

class TrimDemo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '  knk trim   '
        };
    }

    render() {    
        const { data } = this.state;
        return (
            <div>
                <div>
                    <p>初始数据</p>
                    <p className="m-t"><pre>{ data }</pre></p>
                </div>
                <div className="m-t">
                    <p>默认去掉所有空格</p>
                    <p className="m-t"><pre>{ tool.trim(data) }</pre></p>
                </div>
                <div className="m-t">
                    <p>去掉前面空格</p>
                    <p className="m-t"><pre>{ tool.trim(data, 'before') }</pre></p>
                </div>
                <div className="m-t">
                    <p>去掉后面空格</p>
                    <p className="m-t"><pre>{ tool.trim(data, 'after') }</pre></p>
                </div>
                <div className="m-t">
                    <p>去掉前后空格</p>
                    <p className="m-t"><pre>{ tool.trim(data, 'both') }</pre></p>
                </div>
            </div>
        );
    }
}

export default TrimDemo;