---
title: CommonTable 通用表格
group:
  title: 基础组件
  order: 0
nav:
  title: 组件
  order: 1
---

## CommonTable 通用表格

通用表格组件

<code src="../../examples/components/basic/common_table.jsx">

## API

参数(prop)  | 说明(descripton)                        | 类型(type)           | 是否必填(isRequired)      | 默认值(default)
-----------|-----------------------------------------|----------------------|---------------------|------------------------------------
  apiName              | API请求名                                       | `String`    | `true`           |
  apiData        |  api请求附加的数据                       | `Object` | `false` | `{}` 
  apiMethod       |  api请求method           | `String`           | `false`   | `GET`  
  listName    |  缺省列表名称                      | `String` | `true` |
  listValueKey |  列表值的key值     | `String` | `true`     |
  listNameKey |  列表名称key值     | `String`  | `true`     | 
  value             |  值,支持字符串和数组形式    | `String 或者 Array ([key, label])` | `false`
  onChange             |  onChange    | `Function(e:Event, data)` | `false`
  disabled        |  是否可编辑                       | `Boolean` | `false` | `false`