import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'antd';
import { SelectKeyWord } from 'knk-react';

const FormItem = Form.Item;

const keyWords = [
    {
        text: '打折申请ID',
        value: '1'
    }, {
        text: '机会ID',
        value: '2'
    }, {
        text: 'caseID',
        value: '3'
    }
];
class SelectKeyWordDemo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {}
        };
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {      
        const { getFieldDecorator } = this.props.form;
          
        return (
            <Form>
                <Row>
                    <Col span={8}>
                        <FormItem
                            label="关键字"
                        >
                            {getFieldDecorator('keyWord', {
                                initialValue: []
                            })(
                                <SelectKeyWord keyWord={keyWords} />
                            )}
                        </FormItem>
                    </Col>
                </Row>
            </Form>
        );
    }
}

SelectKeyWordDemo.propTypes = {
    form: PropTypes.object.isRequired,
    onSubmit: PropTypes.func,
    defaultValue: PropTypes.object
};

export default Form.create()(SelectKeyWordDemo);