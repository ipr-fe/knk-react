---
title: nrm使用方法
group:
  title: 工具介绍
  order: 0
nav:
  title: 知识库
  order: 3
---

## 为什么要切换npm源

### 1.1 速度太慢

因为默认的npm源是国外的，速度比较慢。可以选择国内镜像，加快下载安装速度，比如我们可以切换到taobao源或者公司内部的源。


### 1.2 手动切换太麻烦

切换源时，往往记不住源链接，百度之后再来执行命令npm config set registry ....


## 推荐使用nrm切换npm源

nrm 是一个 js 模块，是一个 npm 源管理器，是一个命令行工具，可以用来快速切换 npm 源。



什么意思呢，npm默认情况下是使用npm官方源（使用npm config ls命令可以查看），在国内用这个源肯定是不靠谱的，一般我们都会用[淘宝npm源](https://registry.npm.taobao.org/)，修改源的方式也很简单，在终端输入：

```

npm set registry https://registry.npm.taobao.org/

```
再npm config ls查看，已经切换成功。



那么，问题来了，如果哪天你又跑去国外了，淘宝源肯定是用不了的，又要切换回官网源，或者哪天你们公司有自己的私有npm源了，又需要切换成公司的源，这样岂不很麻烦？于是有了nrm。


### 2.1 nrm安装方法

```

npm install -g nrm

```


### 2.2 查看可选npm源

```

nrm ls

```
![](./imgs/nrm_1.png)

### 2.3 切换npm源

```

nrm use npm

```

![](./imgs/nrm_2.png)

### 2.4 增加npm源

```

nrm add hinpm https://registry.npm.taobao.org

```

![](./imgs/nrm_3.png)

### 2.5 删除npm源

```

nrm del hinpm

```

![](./imgs/nrm_4.png)

### 2.6 测试npm源速度

```

nrm test

```

![](./imgs/nrm_5.png)