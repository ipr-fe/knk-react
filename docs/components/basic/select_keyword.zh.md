---
title: SelectKeyWord 关键字搜索
group:
  title: 基础组件
  order: 0
nav:
  title: 组件
  order: 1
---

## SelectKeyWord 关键字搜索

关键字搜索，适用于多个筛选条件的场景

<code src="../../examples/components/basic/select_keyword.jsx">