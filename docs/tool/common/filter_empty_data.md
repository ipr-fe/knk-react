---
title: filterEmptyData 过滤空数据
group:
  title: 通用函数
  order: 0
nav:
  title: 工具函数
  order: 2
---

## filterEmptyData 过滤空数据

```
/**
 * 过滤空数据
 */
export function filterEmptyData(data) {
    if(!data) return data;
    Object.keys(data).forEach((inx) => {
        if (data[inx] === 'undefined' || data[inx] === undefined || data[inx] === null || data[inx] === '' || data[inx].length == 0)
            delete data[inx];
    });
    return data;
}
```
## 原始数据

```
data: {
    userId: 1,
    userName: 'artd',
    gender: '',
    address: null
}
```

## 过滤后数据

<code src="../../examples/tool/common/filter_empty_data.jsx">
