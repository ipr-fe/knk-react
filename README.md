# knk-react

[![npm version][npm-image]][npm-url] [![mnt-image](https://img.shields.io/maintenance/yes/2022.svg?style=flat-square)](../../commits/master) [![Gitee stars](https://gitee.com/ipr-fe/knk-react/badge/star.svg?theme=dark)](https://gitee.com/artdong/knk-react/stargazers)
[![npm download][download-image]][download-url] [![dumi](https://img.shields.io/badge/docs%20by-dumi-blue?style=flat-square)](https://gitee.com/umijs/dumi) [![Gitee license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://gitee.com/ipr-fe/knk-react/blob/master/LICENSE)

[npm-image]: http://img.shields.io/npm/v/knk-react.svg?style=flat-square
[npm-url]: http://npmjs.org/package/knk-react
[download-image]: https://img.shields.io/npm/dm/knk-react.svg?style=flat-square
[download-url]: https://npmjs.org/package/knk-react
[bundlephobia-url]: https://bundlephobia.com/result?p=knk-react
[bundlephobia-image]: https://badgen.net/bundlephobia/minzip/knk-react

> react components based on react

## 📍 如何使用

### 安装

```bash
npm i knk-react
## or
yarn add knk-react
```

### 使用

[快速开始 →](https://ipr-fe.gitee.io/knk-react/)

## 📐 组件看板

- 基础组件
  - [SelectKeyWord 选择关键字](https://ipr-fe.gitee.io/knk-react/components/basic/select_keyword)
  - [SelectTag 选择标签](https://ipr-fe.gitee.io/knk-react/components/basic/select_tag)
  - [AjaxSelect 异步选择](https://ipr-fe.gitee.io/knk-react/components/basic/ajax_select)
  - [CommonTable 通用表格](https://ipr-fe.gitee.io/knk-react/components/basic/common_table)

## 🖥 浏览器兼容性

- 现代浏览器和 Internet Explorer 11 (with [polyfills](https://stackoverflow.com/questions/57020976/polyfills-in-2019-for-ie11))

| ![](https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png)<br>IE / Edge | ![](https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png)<br>Firefox | ![](https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png)<br>Chrome | ![](https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png)<br>Safari |
| ------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------- |
| IE11, Edge                                                                                             | last 2 versions                                                                                            | last 2 versions                                                                                         | last 2 versions                                                                                               |

## LICENSE

[MIT](https://gitee.com/ipr-fe/knk-react/blob/master/LICENSE)