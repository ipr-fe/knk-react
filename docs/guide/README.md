---
nav:
  title: 介绍
  order: 0
toc: menu
---

# 快速上手

### 安装

```
# use npm
npm i knk-react

# use yarn
yarn add knk-react
```

### 使用

[快速开始 →](https://ipr-fe.gitee.io/knk-react/)