---
title: trim 去除字符串空格
group:
  title: 通用函数
  order: 0
nav:
  title: 工具函数
  order: 2
---

## trim 去除字符串空格

```
/**
 * 去除字符串空格
 */
 export function trim(str, position) {
    const type = Object.prototype.toString.call(str).slice(8, -1);
    if(type !== 'String') return str;
    let result = '';
    switch(position) {
        case 'before': // 前
            result = str.replace(/(^\s*)/g, "");
            break;
        case 'after': // 后
            result = str.replace(/(\s*$)/g, "");
            break;
        case 'both': // 前后
            result = str.replace(/(^\s*)|(\s*$)/g, "");
            break;
        default: // 所有
            result = str.replace(/\s*/g,"");
   } 
   return result;
}
```

## 示例

<code src="../../examples/tool/common/trim.jsx">
