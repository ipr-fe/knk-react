/**
 * 选择关联标签
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { Tag } from 'antd';

//封装组件
class SelectTag extends Component {
    render() {
        const {value} = this.props;
        if(!value) return null;
        return (
            <div>
                {value.map((item, index) => {
                    return (
                        <Tag 
                            closable 
                            key={index} 
                            onClose={(e) => this.handleRemove(e, item.value)}
                        >
                            {item.text}
                        </Tag>
                    );
                })}
            </div>
        );
    }

    handleRemove(e, itemVal) {
        const {value, onChange} = this.props;
        e.preventDefault();
        onChange(value.filter(function(item) {
            return item.value !== itemVal;
        }));
    }
}

SelectTag.propTypes = {
    value: PropTypes.array,
    onChange: PropTypes.func
};

export default SelectTag;